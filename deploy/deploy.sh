set -e

eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config
DEPLOY_SERVERS=$DEPLOY_SERVERS
ssh ubuntu@${DEPLOY_SERVERS} 'bash' < ./deploy/updateAndRestart.sh
